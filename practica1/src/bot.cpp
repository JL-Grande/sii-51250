#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
//#include <stdio.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
    int file;                                   //Fichero
    DatosMemCompartida* pMemComp;               //Puntero a DatosMemCompartida
    char* proy;                                 //Puntero proyeccion

    file=open("/tmp/datosBot.txt",O_RDWR);      //Se abre el fichero proyectado en memoria en modo lectura y escritura
    proy=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);    //Se proyecta el fichero

    close(file);                                //Se cierra el fichero

    pMemComp=(DatosMemCompartida*)proy;   //Se apunta el puntero de datos a la proyeccion del fichero en memoria

    //Lógica del bot
    while(1)                                    //Bucle infinito
    {
        float posRaqueta;
        posRaqueta=(pMemComp->raqueta1.y1+pMemComp->raqueta1.y2)/2;

        if(posRaqueta<pMemComp->esfera.centro.y) pMemComp->accion=1;
        else if(posRaqueta>pMemComp->esfera.centro.y) pMemComp->accion=-1;
        else pMemComp->accion=0;

        usleep(25000);
    }

    munmap(proy,sizeof(*(pMemComp)));            //Se cierra la proyección en memoria
}