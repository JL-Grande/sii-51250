// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

char* proy;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	delete [] listaEsfera;

	char cerrar[200];
	sprintf(cerrar,"----Tenis cerrado----");
	write(fifo,cerrar,strlen(cerrar)+1);
	close(fifo);				//Cerrar la tubería
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(int i=0;i<num_esfera;i++) {
			listaEsfera[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	l++;
	if(l==300) {
		l=0;
		if(num_esfera<MAX_ESFERA)
			num_esfera++;
	}
		
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0;i<num_esfera;i++) {
		listaEsfera[i].Mueve(0.025f);
	}

	int i;
	for(int j=0;j<num_esfera;j++) {
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(listaEsfera[j]);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}

		jugador1.Rebota(listaEsfera[j]);
		jugador2.Rebota(listaEsfera[j]);
		if(fondo_izq.Rebota(listaEsfera[j]))
		{
			listaEsfera[j].centro.x=0;
			listaEsfera[j].centro.y=rand()/(float)RAND_MAX;
			listaEsfera[j].velocidad.x=2+2*rand()/(float)RAND_MAX;
			listaEsfera[j].velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;

			//Mensaje del fifo
			char cad[200];
			sprintf(cad,"Jugador 2 marca 1 punto, lleva un total de %d puntos.",puntos2);
			write(fifo,cad,strlen(cad)+1);				//Envío de datos

			listaEsfera[j].radio=0.5f;
		}

		if(fondo_dcho.Rebota(listaEsfera[j]))
		{
			listaEsfera[j].centro.x=0;
			listaEsfera[j].centro.y=rand()/(float)RAND_MAX;
			listaEsfera[j].velocidad.x=-2-2*rand()/(float)RAND_MAX;
			listaEsfera[j].velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;

			//Mensaje del fifo
			char cad[200];
			sprintf(cad,"Jugador 1 marca 1 punto, lleva un total de %d puntos.",puntos1);
			write(fifo,cad,strlen(cad)+1);				//Envío de datos

			listaEsfera[j].radio=0.5f;
		}

		//Implementación del bot
		//Recibimos con un switch case el valor de la acción que se proyecta desde el bot en el fichero.
		switch(pMemComp->accion)
		{
			case 1: OnKeyboardDown('w',1,1); break;
			case 0: break;
			case -1: OnKeyboardDown('s',1,1); break;
		}

		//Debemos actualizar los valores reales en nuestra proyección del fichero
		pMemComp->esfera=listaEsfera[j];
		pMemComp->raqueta1=jugador1;
	}

	//Finalización del juego por límite de puntos
	int estadoActual=0;
	int estadoAnterior=0;
	char cad[200];
	
	if(puntos1==MAX_PUNTOS)
	{
		estadoActual=1;
		if(estadoActual!=estadoAnterior)
		{
			sprintf(cad,"----Jugador 1 ha marcado %d puntos y gana la partida----",MAX_PUNTOS);
			write(fifo,cad,strlen(cad)+1);
		}
		exit(0);
	}
	if(puntos2==MAX_PUNTOS)
	{
		estadoActual=1;
		if(estadoActual!=estadoAnterior)
		{
			sprintf(cad,"----Jugador 2 ha marcado %d puntos y gana la partida----",MAX_PUNTOS);
			write(fifo,cad,strlen(cad)+1);
		}
		exit(0);
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
}

void CMundo::Init()
{
	listaEsfera = new Esfera[MAX_ESFERA];
	num_esfera=1;

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Abrir tuberia en modo escritura en el Init() de Mundo
	fifo=open("/tmp/loggerfifo",O_WRONLY);

	//Fichero proyectado en memoria
	int file=open("/tmp/datosBot.txt",O_RDWR|O_CREAT|O_TRUNC, 0777); 				//Crea el fichero
	write(file,&MemComp,sizeof(MemComp)); 											//Escribe en el fichero
	proy=(char*)mmap(NULL,sizeof(MemComp),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); 	//Asigna al puntero de proyeccion el lugar donde esta proyectado el fichero
	close(file); 																	//Cierra el fichero
	pMemComp=(DatosMemCompartida*)proy; 											//Asigna el valor del puntero de proyeccion al puntero de Datos
	pMemComp->accion=0;																//Establece la accion inicial
}