#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char* regv[])
{
    int exit=0;
    int aux;
    mkfifo("/tmp/loggerfifo",0777);                     //Crear tubería
    int fd=open("/tmp/loggerfifo", O_RDONLY);           //Abrir tubería en modo lectura
    while (exit==0)                                     //Bucle infinito
    {
        char buff[200];
        aux=read(fd,buff,sizeof(buff));                 //Recepción de datos
        printf("%s \n",buff);                           //Imprimir mensaje por salida estandar
        if(buff[0]=='-' || aux==-1)                     //Con aux nos aseguramos una correcta lectura
        {
            printf("----Cerrando logger----\n");
            exit=1;
        }
    }
    close(fd);                                          //Destruir tubería
    unlink("/tmp/loggerfifo");
    return 0;
}